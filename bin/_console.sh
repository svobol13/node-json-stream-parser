#!/usr/bin/env bash

DIR="$( cd "$( dirname "$0" )" && pwd )"

# DOC: Test command that does nothing
if [ "$1" = "test" ]; then
  echo "It Works!"
# DOC: Release new version of application. Tag commit, merge develop to master, back-merge...
elif [ "$1" = "release-develop" ]; then
    set -Eeuo pipefail
    # Check login to npm, if not it does not make sense to continue
    npm whoami >/dev/null 2>&1 || (
      echo "You have to [npm login] first."
      exit 1
    ) || exit 1
    v=$(npm version patch)
    echo "About to release version [$v]."
    rm -rf dist && npm i && npm run test && tsc && npm publish || exit 1
    git push origin ${v}
else
    COMMANDS=($(grep -o -e '= ".*"' ${DIR}/_console.sh | grep -o -e '".*"$' | grep -v '\$' | cut -d '"' -f 2))
    OLD_IFS=$IFS
    IFS=$'\n'
    DOCS=($(grep -e '^# DOC:' ${DIR}/_console.sh | grep -o -e ': .*$'))
    IFS=${OLD_IFS}

    for i in ${!COMMANDS[@]}; do
        DOC=$(echo ${DOCS[$i]} | cut -d ':' -f 2)
        echo $i ' | ' ${COMMANDS[$i]} ' | ' ${DOC}
    done
    exit 0
fi

