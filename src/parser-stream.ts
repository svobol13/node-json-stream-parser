import { Duplex, PassThrough } from 'stream'
import Parser from './parser'
import { Worker } from 'worker_threads'
import path from 'path'

export default class ParserStream extends Duplex {
  private readonly pt: Duplex
  private readonly parser: Parser
  private readonly workerCount: number
  private readonly _workers: Worker[] = []

  /**
   * @param workerCount Specify how many workers to use. Default 0 which means everything happens in main thread.
   * @param inputBuffer Optionally custom binary input buffer might be supplied
   * @param readableHighWaterMark Optionally output capacity might be specified
   */
  constructor (workerCount: number = 0, inputBuffer = new PassThrough(), readableHighWaterMark = 16) {
    super({
      allowHalfOpen: false,
      objectMode: false,
      writableObjectMode: false,
      readableObjectMode: true,
      readableHighWaterMark: readableHighWaterMark,

      write: (chunk: any, encoding: string, callback: (error?: (Error | null)) => void): void => {
        this.pt.write(chunk, encoding, callback)
      },
      destroy (error: Error | null, callback: (error: (Error | null)) => void): void {
        callback(error)
      },
      final (callback: (error?: (Error | null)) => void): void {
        callback()
      },
    })
    this.workerCount = workerCount
    this.pt = inputBuffer
    this.on('end', () => {
      this.pt.end()
    })
    this.parser = new Parser(this.pt[Symbol.asyncIterator](), 1024 * 1024 * 50, 1)

    // Init workers
    for (let i = 0; i < this.workerCount; i++) {
      const worker = new Worker(path.join(__dirname, 'workerwrapper.js'))
      worker.on('message', (obj) => {
        this._counter--
        this._push(obj)
      })
      this._workers.push(worker)
    }
  }

  private _currentWorker = 0
  private _endWatchDog = 0
  private _counter = 0
  private _reading = false

  private get worker () {
    return this._workers[this._currentWorker = ++this._currentWorker % this._workers.length]
  }

  async _read (size: number): Promise<void> {
    if (this._reading) {
      return
    }
    this._reading = true
    let nextObject
    while (nextObject = await this.parser.nextObject()) {
      this._counter++
      if (this.workerCount) {
        this.worker.postMessage(nextObject)
      } else {
        this._counter--
        this._push(ParserStream.parse(ParserStream.stringify(nextObject)))
      }
      if (!this._reading) {
        break
      }
    }
    // Finish
    await this.finish()
  }

  private _push (o: Object | null) {
    this._reading = this._reading && this.push(o)
  }

  // Finalizer
  private async finish () {
    if (this._endWatchDog > 100) {
      throw new Error(`Something went wrong. Did not receive expected amount of objects and [${this._counter}] left.`)
    }
    if (this._counter === 0) {
      this._push(null)
      for (let worker of this._workers) {
        await worker.terminate()
      }
    } else {
      this._endWatchDog++
      setTimeout(() => {
        this.finish()
      }, 100)
    }
  }

  private static stringify (buf: Buffer): string {
    return buf.toString()
  }

  private static parse (str: string): Object {
    return JSON.parse(str)
  }

}
