import * as fs from "fs"
import ParserStream from './parser-stream'
import { parse } from 'jsonstream'
import _ from 'lodash'
import { PassThrough } from 'stream'
import { createGunzip } from 'zlib'

async function app() {
  for (let file of ['MACZ', 'MASK', 'MAPL', 'MASI', 'MAHU', 'MAHR']) {
    try {
      await f(file)
    } catch (e) {
      console.log(`Error [${e}] while processing [${file}].`)
    }
  }
}

async function f (file: string) {
  const newParser = new ParserStream()
  const oldParser = parse('data.*')

  const oldIterable = fs.createReadStream(`datasources/${file}.json.gz`).pipe(createGunzip()).pipe(oldParser).pipe(new PassThrough({objectMode: true}))[Symbol.asyncIterator]()
  const newIterable = fs.createReadStream(`datasources/${file}.json.gz`).pipe(createGunzip()).pipe(newParser).pipe(new PassThrough({objectMode: true}))[Symbol.asyncIterator]()

  let old
  let neu
  let compared = 0
  let elapsed = 0
  const interval = setInterval(() => {
    console.log(`Compared [${compared}] items with average speed [${Math.round(compared / elapsed++)}] items per second after [${elapsed}]seconds`)
  }, 1000)
  while (true) {
    compared++
    old = await oldIterable.next()
    neu = await newIterable.next()
    // continue
    if (!_.isEqual(old.value, neu.value)) {
      console.error(old)
      console.error(neu)
      process.exit(1)
      break
    }
    if (!old || !neu) {
      break
    }
  }
  clearInterval(interval)
  console.log(`All nicely done.`)
}

app().catch(console.error)
