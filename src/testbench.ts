import * as fs from 'fs'
import { Duplex } from 'stream'
import Parser from './parser'

async function f () {
  const rs = fs.createReadStream('MAHR.json')
  const pp = new Parser(rs[Symbol.asyncIterator](), 1024 * 1024 * 10, 1)
  let o
  let i = 0
  let sec = 0

  function log() {
    console.log(`Processed [${i}] speed [${Math.floor(i / sec++)}] products per second.`)
  }

  const interval = setInterval(log, 1000)

  try {
    while (o = await pp.nextObject()) {
      i++
      if (i === 133733) {
        let a = 9
      }
      JSON.parse(o.toString())
    }
  } catch (e) {
    console.error(o.toString())
    console.error(e)
    throw e
  } finally {
    log()
    clearInterval(interval)
  }
}

f().catch(console.error)

