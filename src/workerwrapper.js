import { parentPort } from 'worker_threads'
import { TextDecoder } from 'util'

const td = new TextDecoder()
parentPort.on('message', (buf) => {
  parentPort.postMessage(JSON.parse(td.decode(buf)))
})
