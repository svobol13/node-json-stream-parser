export default class Parser {

    private buf: Buffer
    private readonly srcIt: AsyncIterator<Buffer>

    private src: Buffer = Buffer.allocUnsafe(0)
    private srcPos: number = 0

    private bufPos: number = 0
    private objStart: number = -1
    private objDepth: number = 0
    private bufEnd: number = 0

    private readonly ARR_OPEN = '['.codePointAt(0)
    private readonly ARR_CLOSE = ']'.codePointAt(0)
    private readonly OBJ_OPEN = '{'.codePointAt(0)
    private readonly OBJ_CLOSE = '}'.codePointAt(0)
    private readonly DOUBLE_QUOTE = '"'.codePointAt(0)
    private readonly ESCAPE = '\\'.codePointAt(0)
    private srcDone: boolean = false

    constructor (source: AsyncIterable<Buffer>, bufSize: number = 1024 * 5, skip: number = 0) {
        this.buf = Buffer.allocUnsafe(bufSize)
        this.srcIt = source[Symbol.asyncIterator]()
        this.bufPos = skip - 1
    }

    private async loadMore () {
        let spaceLeftInBuf = this.buf.length - (this.bufEnd)
        if (spaceLeftInBuf === 0) {
            if (this.objStart !== 0) {
                this.bufferShift()
                spaceLeftInBuf = this.buf.length - (this.bufEnd)
            } else {
                throw Error(`Cannot load more into buffer of size [${this.buf.length}].`)
            }
        }
        if (this.srcPos === this.src.length) {
            const next = await this.srcIt.next()
            this.src = next.value || Buffer.allocUnsafe(0)
            this.srcDone = next.done || false
            this.srcPos = 0
        }
        const loadCharacters = Math.min((spaceLeftInBuf), (this.src.length - this.srcPos))
        this.src.copy(this.buf, this.bufEnd, this.srcPos, this.srcPos += loadCharacters)
        this.bufEnd += loadCharacters
    }

    private bufferShift() {
        if (this.objStart < 0) {
            this.bufPos = -1
            this.bufEnd = 0
            return
        }
        const newBuf = Buffer.allocUnsafe(this.buf.length)
        this.buf.copy(newBuf, 0, this.objStart, this.bufPos + 1)
        this.bufPos -= this.objStart
        this.bufEnd -= this.objStart
        this.objStart = 0
        this.buf = newBuf
    }

    public async nextObject (): Promise<Buffer | null> {
        let skippingString = false
        let escaping = false
        while (true) {
            if ((this.bufPos + 1) >= this.bufEnd) {
                await this.loadMore()
                if (this.srcDone && this.objStart < 0) {
                    return null
                }
            }
            switch (this.buf[++this.bufPos]) {
                case this.ESCAPE:
                    if ((this.bufPos + 1) >= this.bufEnd) {
                        await this.loadMore()
                        if (this.srcDone && this.objStart < 0) {
                            throw Error("Escape cannot be last character.")
                        }
                    }
                    ++this.bufPos // skip
                    break
                case this.DOUBLE_QUOTE:
                    skippingString = !skippingString
                    break
                case this.OBJ_OPEN:
                    if (skippingString) break
                    this.objDepth++
                    if (this.objDepth === 1) {
                        this.objStart = this.bufPos
                    }
                    break
                case this.OBJ_CLOSE:
                    if (skippingString) break
                    this.objDepth--
                    if (this.objDepth === -1) {
                        // TODO test this case:
                        // Hack-finished since first was skipped. Once path/object selection implemented this might be removed.
                        this.srcDone = true
                        this.bufPos = 0
                        this.bufEnd = 0
                        this.buf = Buffer.allocUnsafe(0)
                        return null
                    }
                    if (this.objDepth > 0) {
                        break
                    }
                    // profiling: 7% (14.0 / 14.8)
                    const objAsBuff = Buffer.allocUnsafe(this.bufPos - this.objStart + 1)
                    // profiling: 5.7% (13.25 / 14)
                    this.buf.copy(objAsBuff, 0, this.objStart, this.bufPos + 1)
                    this.objStart = -1
                    return objAsBuff
                default:
            }
        }
    }
}
