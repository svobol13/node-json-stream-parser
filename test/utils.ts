import Parser from '../src/parser'
import { Readable } from 'stream'

export async function testIt (parts: string[], expectObjects: number, buffSize = 50, skip = 0) {
  console.log(`Test [${JSON.stringify(arguments)}] begun.`)

  async function * it () {
    for (let part of parts) {
      yield Buffer.from(part)
    }
  }

  const p = new Parser(it(), buffSize, skip)
  for (let i = 0; i < expectObjects; i++) {
    JSON.parse((await p.nextObject() || '{]').toString())
  }
  if ((await p.nextObject()) !== null) {
    throw Error(`There must be exactly [${expectObjects}] but more found.`)
  }
  console.log(`Test [${JSON.stringify(arguments)}] succeeded.`)
}

export async function sleep (milis: number) {
  await new Promise(resolve => setTimeout(resolve, milis))
}

export class ConfigurableStream extends Readable {
  private _checksum: number = 0
  private _counter: number = 0
  private readonly _maxItems: number
  private _iterable = this.it()

  constructor (maxItems: number = 2) {
    super({
      objectMode: false,
      autoDestroy: false,
    })

    if (maxItems < 2) {
      throw new Error(`Max items must be more than [1] but [${maxItems}] set.`)
    }
    this._maxItems = maxItems
  }

  async _read (size: number): Promise<void> {
    do {
      const { done, value } = await this._iterable.next()
      if (done) {
        this.push(null)
        break
      }
      if (value) {
        size -= value.length
        const ret = this.push(value)
        if (!ret) {
          break
        }
      }
    } while (size > 0)
  }

  get counter () {
    return this._counter
  }

  get checksum () {
    return this._checksum
  }

  private async * it () {
    yield `{"data": [${this.nextObject}`
    while (this._counter < (this._maxItems - 1)) {
      yield `,${this.nextObject}`
    }
    yield `,${this.nextObject}]}`
  }

  private get nextObject (): string {
    const number = Math.round(Math.random() * 100)
    this._checksum += number
    return `{"id": ${++this._counter}, "value": ${number}}`
  }

}
