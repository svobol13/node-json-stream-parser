import { testIt } from './utils'

describe('bar', () => {
    it('sync function returns true', async () => {
        await testIt(['[', '{}', ',', '{}', ']'], 2)
        await testIt(['[', '{} , {}', ']  '], 2, 2)
        await testIt(['[', '{},{}', ']'], 2, 2)
        await testIt(['[', '{}, {}', ']'], 2, 2)
        await testIt(['[', '{} , {}', ']'], 2, 2)
        await testIt(['{"a": [{},{}]}'], 1, 20)
        await testIt(['{"a": [{},{}]}'], 2, 20, 1)
        await testIt(['[', '{},', '{"id":{}}', ']'], 2, 1000)
        await testIt(['[', '{},', '{"i{d":{}}', ']'], 2, 1000)
        await testIt(['[', '{},', '{"id":"{}}"}', ']'], 2, 1000)
        await testIt(['[{"id": "{"}]'], 1)
        await testIt(['[{"id": "\\"{"}]'], 1)
        await testIt(['[{"id": "\{"}]'], 1)
        await testIt(['[{"id": "\\\\"}]'], 1)
    })
})

