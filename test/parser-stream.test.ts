import { ConfigurableStream, sleep } from './utils'
import ParserStream from '../src/parser-stream'
import { PassThrough } from 'stream'
import assert from 'assert'

async function testInputLeak (highWaterMark: number, predicate: (length: number) => boolean, maxItems: number) {
    const s = new ConfigurableStream(maxItems)
    const inputBuffer = new PassThrough({
        highWaterMark: highWaterMark
    })
    const p = new ParserStream(0, inputBuffer)
    s.pipe(p)
    await sleep(200)
    assert(predicate(inputBuffer['_readableState'].length), `Leak detected - [${s.counter}] items cannot fit into given buffer.`)
}

async function testOutputLeak (maxItems: number, outputBufferHighWatermark: number, predicate: (length: number) => boolean) {
    const s = new ConfigurableStream(maxItems)
    const inputBuffer = new PassThrough({
        highWaterMark: 50
    })
    const outputBuffer = new PassThrough({
        objectMode: true,
        highWaterMark: outputBufferHighWatermark
    })
    const p = new ParserStream(0, inputBuffer)
    s.pipe(p).pipe(outputBuffer)
    await sleep(200)
    assert(p['_readableState'].highWaterMark >= p['_readableState'].length, `High water mark not respected. Expected max [${p['_readableState'].highWaterMark}] items but found [${p['_readableState'].length}].`)
    assert(predicate(outputBuffer['_readableState'].length), `Leak detected - [${s.counter}] items cannot fit into given buffer.`)
}

describe('that stream wrapper respects backpressure', () => {
    it('should not leak on input side', async () => {
        await testInputLeak(50, length => length < 100, 5000)
        await testInputLeak(500, length => length > 100, 5000)
        await testInputLeak(50, length => length < 100, 2)
    })
    it('should not leak on output side', async () => {
        await testOutputLeak(5000, 3, length => length === 3)
        await testOutputLeak(5, 10, length => length < 9)
    })
})

